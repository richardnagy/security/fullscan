from os import getenv
from setuptools import setup, find_packages

def read_file(name, default=None):
    try:
        with open(name, 'r', encoding='utf8') as file:
            return file.read()
    except IOError:
        return default

# Read version from pipeline commit tag
# Otherwise fall back to version.txt
# Then fall back to 0.1
version = getenv('CI_COMMIT_TAG', 'v0.1')[1:]
if version == '0.1':
    read_file('version.txt', None)
else:
    with open('version.txt', 'w', encoding='utf8') as f:
        f.write(version)

# Read text from files
description = read_file('description.md', '')
requirements = read_file('requirements.txt', []).splitlines()

setup(
    name = "fullscan",
    version = version,
    author = "Richard Antal Nagy",
    author_email="nagy.richard.antal@gmail.com",
    description="Python CLI application to do a progressively in-depth full port scan on a remote computer.",
    license = "MIT",
    keywords = [ "nmap", "scan", "port scan", "cli" ],
    url = "https://gitlab.com/richardnagy/security/fullscan",
    packages=find_packages(),
    classifiers=[
        "Intended Audience :: Developers",
        "Intended Audience :: Information Technology",
        "Topic :: Security",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=requirements,
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'fullscan = fullscan.main:main'
        ]
    },
    python_requires='>=3.8',
    long_description_content_type='text/markdown',
    long_description=description
)
