import sys
from .scan import full_scan

def main():
    if len(sys.argv) < 2:
        print("Missing parameter: host")
        sys.exit(1)

    host = sys.argv[1]
    full_scan(host)

if __name__ == '__main__':
    main()
