import nmap3

from .utils import color_str

colors = {
    'success': (38, 182, 82),
    'warning': (255, 211, 0),
    'failure': (250, 41, 41),
}

nmap = nmap3.Nmap()

def clean_scan_result(result):
    ip = None
    for key in result:
        if key not in ['stats', 'runtime']:
            ip = key
            break

    result['ports'] = result[ip]['ports']
    del result[ip]

    return result

def port_state_str(port_state):
    state = None
    match port_state:
        case 'open':
            state = color_str(colors['success'], '  open  ')
        case 'filtered':
            state = color_str(colors['warning'], 'filtered')
        case 'closed':
            state = color_str(colors['failure'], ' closed ')
        case _:
            state = color_str(colors['failure'], '  ????  ')
    return state

def print_port_scan(scan_result):
    state = port_state_str( scan_result['state'] )
    port = scan_result['portid']
    protocol = scan_result['protocol']
    service = scan_result['service']['name']
    print(f'[{state}] {port:5} ({protocol}) {service}')

def full_scan(host):
    results = clean_scan_result( nmap.scan_top_ports(host, 1000) )
    ports = results['ports']
    #print(results['ports'])
    for p in ports:
        if p['state'] != 'closed':
            print_port_scan(p)
