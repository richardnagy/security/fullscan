def printc(color: tuple[int,int,int], text: str) -> None:
    print( color_str(color, text) )

def color_str(color: tuple[int,int,int], text: str) -> str:
    return f"\033[38;2;{color[0]};{color[1]};{color[2]}m{text}\033[0m"
