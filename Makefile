demo_host = 127.0.0.1
deps := requirements.txt

# Environment detection
py := python3
sudo := sudo
ifeq ($(OS),Windows_NT)
	py := python
	sudo := 
endif

# Default make command to start a test
default::
	@$(MAKE) -s install
	@echo "==============="
	@$(MAKE) -s run

# Run a test with the demo values
run::
	fullscan $(demo_host)

# Clean generated files and folders
clean::
	-@$(sudo) rm -rf build/ dist/ *.egg-info fullscan/__pycache__/

# Install package
install::
	$(sudo) $(py) setup.py install

# Create package
pack::
	$(py) setup.py sdist

# Create package
lint::
	pylint fullscan


# Packages
packages: $(deps)
	pip install -r $(deps)